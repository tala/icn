from tkinter import LEFT, Button, Entry, Frame, Label, Tk

# def afficher_mot():
#    global truc
#    mot = champ_nom.get()
#    truc.config(text= )


fen = Tk()
fen.title("devinette")
affichage_devinette = Label(fen, text="P O _ I _ O _")
#affichage_devinette.config(font=("Courier", 12))
affichage_devinette.pack()

bloc_reponse = Frame(fen)
etiquette_reponse = Label(bloc_reponse, text="réponse :")
etiquette_reponse.pack(side=LEFT)
champ_reponse = Entry(bloc_reponse)
champ_reponse.pack(side=LEFT)
bloc_reponse.pack()

bloc_lettre = Frame(fen)
etiquette_lettre = Label(bloc_lettre, text="lettre :")
etiquette_lettre.pack(side=LEFT)
champ_lettre = Entry(bloc_lettre)
champ_lettre.pack(side=LEFT)
bloc_lettre.pack()

case_message = Label(fen, text="Il reste 2 essais")
case_message.pack()

bouton_valider = Button(fen, text="Valider")
bouton_valider.pack()

bouton_quitter = Button(fen, text="Quitter", command=fen.destroy)
bouton_quitter.pack()


fen.mainloop()
